import os
import csv
import time
import calendar
import re
import datetime
from functools import wraps
from itertools import islice
from cStringIO import StringIO
try:
    from collections import OrderedDict
except ImportError:
    from odict import OrderedDict

from flask import Flask, render_template, redirect, url_for, request, session, Response, json, make_response, abort
import pymongo
from pymongo import MongoClient, DESCENDING
from pymongo.database import Database
from bson import json_util
from bson.objectid import ObjectId
from jinja2 import Markup

import choicewords

MONGODB_URL = os.environ.get('MONGOLAB_URI', 'mongodb://localhost:27017/proxyface')

#### Database ####

def connect_to_database():
    global db
    client = MongoClient(MONGODB_URL)
    db_name = MONGODB_URL[MONGODB_URL.rindex('/')+1:]
    db = client[db_name]

#### App Initialization ####

app = Flask(__name__)

#### Status ####

@app.route('/status', methods=['POST'])
def post_status():
    message = request.form.get('message')
    print message
    status = {}
    status['message'] = message
    status['status'] = 'queued'
    status['created'] = time.time()
    db.statuses.save(status)

    accounts = list(db.accounts.find())
    for account in accounts:
        task = {}
        task['account_email'] = account['email']
        task['account_password'] = account['password']
        task['message'] = message
        task['status'] = 'queued'
        task['created'] = time.time()
        db.tasks.save(task)

    return redirect('/')

#### Tasks ####

@app.route('/tasks/<task_id>')
def tasks_detail(task_id):
    task = db.tasks.find_one({'_id': ObjectId(task_id)})
    print task
    return render_template('tasks_detail.html', task=task)

@app.route('/tasks/retry', methods=['POST'])
def tasks_retry():
    task_id = request.form.get('task_id')
    task = db.tasks.find_one({'_id': ObjectId(task_id)})
    print 'retrying', task
    new_task = {}
    new_task['account_email'] = task['account_email']
    new_task['account_password'] = task['account_password']
    new_task['message'] = task['message']
    new_task['status'] = 'queued'
    new_task['created'] = time.time()
    db.tasks.save(new_task)
    return redirect('/')

#### Accounts ####

@app.route('/accounts/create', methods=['POST'])
def create_account():
    email = request.form.get('email')
    password = request.form.get('password')
    account = {
        'email': email,
        'password': password
    }
    db.accounts.save(account)
    return redirect('/')

@app.route('/')
def index():
    statuses = list(db.statuses.find())
    accounts = list(db.accounts.find())
    tasks = list(db.tasks.find().sort('_id', pymongo.DESCENDING))[:100]
    demo_message = choicewords.from_file("status.txt")
    return render_template('index.html', statuses=statuses, accounts=accounts, tasks=tasks, demo_message=demo_message)

if __name__=='__main__':
    connect_to_database()
    app_port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=app_port, debug=True)
