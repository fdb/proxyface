## Installation (Mac)
- Download Selenium Standalone Server: http://goo.gl/PJUZfa
- In Terminal, run:

    sudo easy_install selenium

## Installation  on Windows
- Install Java
- Install Python 2.7
- Open a command prompt, run:

    C:\Python27\Scripts\easy_install selenium
    C:\Python27\Scripts\easy_install pymongo
    C:\Python27\Scripts\easy_install pypiwin32
    C:\Python27\Scripts\easy_install requests
    C:\Python27\Scripts\easy_install flask

## Running on Mac

    export FACEBOOK_EMAIL="your@email.com"
    export FACEBOOK_PASSWORD="your_password"
    python runner.py

## Running on Windows



In one command prompt, run the selenium driver:

    java -jar selenium-server-standalone-2.48.2.jar

In a second command prompt, run the runner.py:

    C:\Python27\python.exe runner.py
