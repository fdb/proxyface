# Run the proxyface scripts at irregular intervals.
import time
from random import uniform
import subprocess

SECONDS = 1
MINUTES = 60
HOURS = 60 * MINUTES

while True:
  t = uniform(1, 5)
  print "Sleeping for %.2f minutes..." % t
  time.sleep(t * MINUTES)
  subprocess.call(["python", "proxyface_slave.py"])
