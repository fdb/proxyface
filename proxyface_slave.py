# This script checks if there are status messages to be posted, connects to Facebook and posts them.
# Since the Facebook API doesn't support posting status updates,
# we use Selenium (http://www.seleniumhq.org/).

import csv
import os
import time
import sys
import traceback

from pymongo import MongoClient

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

MONGODB_URL = os.environ.get('MONGOLAB_URI', 'mongodb://localhost:27017/proxyface')

def find_element_by_placeholder(driver, placeholder, element="textarea"):
  return driver.find_element(By.XPATH, '//' + element + '[@placeholder="' + placeholder + '"]')

def connect_to_database():
    global db
    client = MongoClient(MONGODB_URL)
    db_name = MONGODB_URL[MONGODB_URL.rindex('/')+1:]
    db = client[db_name]

def post_status(driver, task):
  print task
  message = task['message']

  # Implicitly wait for a few seconds if elements are not available.
  # https://selenium-python.readthedocs.org/waits.html
  driver.implicitly_wait(5)

  # Login
  print("Logging in...")
  driver.get("http://www.facebook.com/")
  email_el = driver.find_element_by_id("email")
  email_el.send_keys(task['account_email'])
  pwd_el = driver.find_element_by_id("pass")
  pwd_el.send_keys(task['account_password'])
  pwd_el.send_keys(Keys.RETURN)
  time.sleep(2)

  # Make sure we're on the right page
  print("Switching to homepage...")
  driver.get("http://www.facebook.com/")

  # Write a screenshot to verify that we're good
  time.sleep(2)
  driver.save_screenshot("screenshot-%s-%s.png" % (task['account_email'], time.strftime("%Y-%m-%d_%H.%M.%S")))

  # Post a status update
  print("Posting update...")
  print message
  try:
    ta = find_element_by_placeholder(driver, "What's on your mind?")
  except:
    ta = find_element_by_placeholder(driver, "Wat ben je aan het doen?")

  ta.send_keys(message)
  ta.submit()

  # Write a screenshot to verify that we're good
  time.sleep(2)
  driver.save_screenshot("screenshot-%s-%s.png" % (task['account_email'], time.strftime("%Y-%m-%d_%H.%M.%S")))

  # Quit
  time.sleep(2)

connect_to_database()
while True:
  print "Checking for new tasks..."
  tasks = list(db.tasks.find({'status': 'queued'}))
  for task in tasks:
    task['status'] = 'in_progress'
    db.tasks.save(task)
    driver = None
    try:
      driver = webdriver.Firefox(timeout=10)
      post_status(driver, task)
    except Exception, e:
      print e
      task['error'] = repr(e)
      task['stack'] = ''.join(traceback.format_stack())
      task['status'] = 'error'
    else:
      task['status'] = 'posted'
    if driver is not None:
      try:
        driver.quit()
      except Exception, e:
        print e
    db.tasks.save(task)
    time.sleep(1)
  time.sleep(10)
