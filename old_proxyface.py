# This script automatically connects to Facebook and posts messages.
# Since the Facebook API doesn't support posting status updates,
# we use Selenium (http://www.seleniumhq.org/).

import os
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

import choicewords

FACEBOOK_EMAIL = os.environ['FACEBOOK_EMAIL']
FACEBOOK_PASSWORD = os.environ["FACEBOOK_PASSWORD"]

def find_element_by_placeholder(driver, placeholder, element="textarea"):
  return driver.find_element(By.XPATH, '//' + element + '[@placeholder="' + placeholder + '"]')

driver = webdriver.Firefox()

# Implicitly wait for a few seconds if elements are not available.
# https://selenium-python.readthedocs.org/waits.html
driver.implicitly_wait(5)

# Login
print("Logging in...")
driver.get("http://www.facebook.com/")
email = driver.find_element_by_id("email")
email.send_keys(FACEBOOK_EMAIL)
pwd = driver.find_element_by_id("pass")
pwd.send_keys(FACEBOOK_PASSWORD)
pwd.send_keys(Keys.RETURN)
time.sleep(2)


# Make sure we're on the right page
print("Switching to homepage...")
driver.get("http://www.facebook.com/")

# Write a screenshot to verify that we're good
time.sleep(2)
driver.save_screenshot("screenshot-%s.png" % time.strftime("%Y-%m-%d_%H.%M.%S"))

# Post a status update
print("Posting update...")
message = choicewords.from_file("status.txt")
print message
try:
  ta = find_element_by_placeholder(driver, "What's on your mind?")
except:
  ta = find_element_by_placeholder(driver, "Wat ben je aan het doen?")
  
ta.send_keys(message)
ta.submit()

# Write a screenshot to verify that we're good
time.sleep(2)
driver.save_screenshot("screenshot-%s.png" % time.strftime("%Y-%m-%d_%H.%M.%S"))

# Quit
time.sleep(2)
driver.quit()
