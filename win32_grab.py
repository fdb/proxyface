import sys
import time
import traceback

import win32gui, win32ui, win32con, win32api
import requests

FILENAME = 'screenshot.bmp'

def take_screenshot():
    print "Taking screenshot..."
    try:
        hwin = win32gui.GetDesktopWindow()
        width = win32api.GetSystemMetrics(win32con.SM_CXVIRTUALSCREEN)
        height = win32api.GetSystemMetrics(win32con.SM_CYVIRTUALSCREEN)
        left = win32api.GetSystemMetrics(win32con.SM_XVIRTUALSCREEN)
        top = win32api.GetSystemMetrics(win32con.SM_YVIRTUALSCREEN)
        hwindc = win32gui.GetWindowDC(hwin)
        srcdc = win32ui.CreateDCFromHandle(hwindc)
        memdc = srcdc.CreateCompatibleDC()
        bmp = win32ui.CreateBitmap()
        bmp.CreateCompatibleBitmap(srcdc, width, height)
        memdc.SelectObject(bmp)
        memdc.BitBlt((0, 0), (width, height), srcdc, (left, top), win32con.SRCCOPY)
        bmp.SaveBitmapFile(memdc, 'screenshot.bmp')
    except:
        print "Grab Error:"
        traceback.print_exc()



def upload_screenshot():
    print "Uploading screenshot..."
    URL = 'http://localhost:5000/screenshot'
    try:
        files = {'file': open(FILENAME, 'rb')}
        r = requests.post(URL, files=files)
        print "Done."
    except:
        print "Upload Error:"
        traceback.print_exc()

if len(sys.argv) < 2:
    print "Usage: python win32_grab.py [remote | local]"
    sys.exit(1)

mode = sys.argv[1]

while True:
    take_screenshot()
    if mode == 'remote':
        upload_screenshot()
    print "Sleeping for 2 seconds..."
    time.sleep(2)

