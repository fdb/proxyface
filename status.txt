root:
- Ik voel me {{ gevoel }} {{ tijdstip }}. {{ vooractie }} {{ sporten }}.
- Telkens wanneer ik me {{ gevoel }} voel spreek ik met {{ naam }} af.
- Ik voel me {{ gevoel }} {{ tijdstip }}.
- Ik ben net gaan {{ sporten }}. Voel me nu {{ gevoel }}{{ punt }}
- Gaan {{ sporten }}. {{ voelme }} {{ gevoel }}{{ punt }}
- Na het {{ sporten }} ga ik {{ dikwijls }} {{ sporten }}. Alleen {{ tijdstip }} {{ excuus }}{{ punt }}
- Samen met {{ naam }} gaan {{ sporten }}.
- {{ naam }} voelde zich vandaag {{ gevoel }}. Nu voel ik me {{ gevoel }}.
- Het leek of {{ naam }} {{ tijdstip }} erg {{ gevoel }} was.
- Helemaal {{ gevoel }} over {{ tijdstip }}. Even {{ naam }} {{ contacteren}}.

voelme:
- Voel me
- Ik ben helemaal
- Helemaal
- Ik ben

punt:
- .
- ...
-  :-)
-  :-(
-  ;)
- !
- ?
- !!

gevoel:
- goed
- slecht
- belabberd
- ziek
- vrij
- bangelijk
- stil
- gelukkig
- opgetogen
- kalm
- tevreden
- nerveus
- futloos
- onzeker
- apathisch
- energiek
- geestdriftig
- verlangend
- gelukkig
- enthousiast
- gelukzalig
- verrast
- gerust
- helder
- geanimeerd
- hartstochtelijk
- versterkt
- kalm
- gepassioneerd
- opgewekt
- verwonderd
- ontspannen
- geprikkeld
- opgewonden
- vurig
- opgelucht
- levendig
- stralend
- op mijn gemak
- verbaasd
- uitbundig
- vrolijk
- rustig
- verbluft
- uitgelaten
- blij
- sereen
- verlangend
- verrukt frivool
- stil
- verrast
- geamuseerd
- tevreden
- versterkt
- geinspireerd
- gelukkig
- vervuld
- verwonderd

weinig:
- geen
- te weinig
- bijna geen
- nauwelijks
- weinig

veel:
- te veel
- teveel
- veel
- veeeel
- nogal wat
- een berg

excuus:
- {{ weinig }} tijd
- {{ weinig }} energie
- pijn gedaan
- {{ veel }} werk
- voel ik me {{ gevoel }}

contacteren:
- bellen
- skypen
- Whatsappen
- messagen
- snapchatten
- teksten
- sms'en

dikwijls:
- normaal altijd
- altijd
- normaal
- dikwijls
- soms
- nooit
- geregeld
- vaak

naam:
- Sophie
- Emma
- Julia
- Mila
- Tess
- Isa
- Zoe
- Anna
- Eva
- Sara
- Fenna
- Evi
- Lotte
- Lynn
- Lisa
- Fleur
- Saar
- Sarah
- Lieke
- Noa
- Roos
- Nora
- Sofie
- Maud
- Sanne
- Yara
- Liv
- Esmee
- Noor
- Olivia
- Amy
- Milou
- Elin
- Nina
- Femke
- Anne
- Jasmijn
- Feline
- Emily
- Naomi
- Nova
- Lois
- Jill
- Luna
- Vera
- Liz
- Eline
- Floor
- Iris
- Lina

tijdstip:
- vandaag
- deze ochtend
- vanmorgen
- vanavond

vooractie:
- Tijd om te
- Goed idee dat ik ga
- Nu ga ik
- Dan ga ik {{ dikwijls }}
- Misschien moet ik gaan
- Wat denken jullie van
- Klaar om te

sporten:
- zwemmen
- fietsen
- lopen
- discuswerpen
- squashen
- volleyballen
- valschermspringen
- zweefvliegen
- voetballen
- discuswerpen
- hoogspringen
- hamerslingeren
- badmintonnen
- tennissen
- bowlen
- kleiduifschieten
- pokeren
- dammen
- klaverjassen
- schaken
- scrabblen
- paalwerpen
- touwtrekken
- racen
- paardrijden
- geocachen
- fitnessen
- joggen
- paintballen
- boogschieten
- mountainbiken
- steppen
- wielrennen
- turnen
